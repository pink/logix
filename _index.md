---
title: Home
---
# card10


Welcome to card10 Wiki. You can [browse](https://git.card10.badge.events.ccc.de/card10/logix) this wiki source and [read more](/en/gitlab) about our GitLab instance.


## Announcements
### New GitLab Server 2019-06-04

We moved from our [Hamburg GitLab](https://gitlab.hamburg.ccc.de/card10/) to:

> [`https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)

1. Please be aware this hosting is a work in progress. If you encounter any issues please let us know on Matrix or IRC (see below).
2. We are aware and working on GDPR compliance, for progress you can track this [issue](https://git.card10.badge.events.ccc.de/card10/meta/issues/1)
3. Social-Sign-On (Twitter, Github, ...) is coming soon

## Overview

> <img class="center" alt="Photo of the card10 badge" src="/media/frontpage-nice-image.jpg"  width="420" height="auto" align="center">
>
> Hello, my name is card10


- Community
  - at [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)): [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr)
  - or [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) (mirror): [`freenode.com#card10badge`](ircs://chat.freenode.net:6697/card10badge)
  - or [GitLab: `https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)
- Social Media
  - [`@card10badge@chaos.social`](https://chaos.social/@card10badge)
  - [`twitter.com/card10badge`](https://twitter.com/card10badge)
- [FAQ](/en/faq)
- Events
  - Upcoming:
     - Card10 meetup in Berlin: xHain 14.06. 19:00
  - Previous
     - CCCamp 2019 Badge at #eh19 (easterhegg2019): [Description](https://conference.c3w.at/eh19/talk/DA7KTT/), [Recording](https://media.ccc.de/search/?q=CCCamp+Badge+Talk)
     - CCCamp2019 Badge Workshop at #eh19: [Description](https://conference.c3w.at/eh19/talk/VHFMJ7/), 2019-04-21, 14:30–15:30
     - Card10 at the [GPN19](https://entropia.de/GPN19:There_will_have_been_a_camp_badge:_We%27re_reconstructing_future_technology_and_you_can_help_us_with_this_mission)
- [Prototypes and prototype ambassadors](/en/prototype_distribution)

## Components

**!Caution!** Card10 is still in the prototyping phase, bear in mind that hardware and PCB layout changes may still occur.

- [Hardware](http://git.card10.badge.events.ccc.de/card10/hardware)
- [LogBook](/en/logbook)
- [Interhacktions](/en/interhacktions)
- [Fundamental board](/en/fundamental-board-overview)
- [usb-c](/en/usbc)
