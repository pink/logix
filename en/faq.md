---
title: FAQ
---

## What can I contribute?
Generally, there is a wide range of contributions that can be made to the firmware, the IOs and Android apps, the various methods of BLE communication, the user interface, the documentation, by reviewing existing work, and of course by creating your own [interhacktions](/en/interhacktions)!

If you don't know where to get started you can also have a look at some of the ideas for contributions we outlined [here](https://gitlab.hamburg.ccc.de/card10/logix/issues). If you have questions on the issue, please don't hesitate to comment or get in touch to the creator of the issue through [Matrix Chat or IRC](/).

## I can't fork any of the repositories!?
Sadly with the current gitlab setup this key feature is not available. We are setting up a new server to solve this problem. In the meantime, you can get in touch via [Matrix Chat or IRC](/).

## How does the so-called wiki work?
Not as well as it should ;) To keep things easier to maintain (the r0ket wiki has been going for 8 years now!), we decided on a git/markdown based wiki for this year. Whilst we are still setting up a gitlab with easy editing access, in the meantime please create a user account for gitlab.hamburg.ccc.de, and contact lilafisch on [Matrix Chat or IRC](/) to get write access to the wiki repo.

Currently, it is easiest to edit the 'wiki' by cloning the git repo, and editing the markdown files. If you are using figures, please place them in media folder. The [fundamental board](/en/fundamental-board-overview) page is an example including image links and page links.

## What components are on card10?
Have a look at the pdf export of the [schematics for the prototype](https://gitlab.hamburg.ccc.de/card10/hardware/blob/master/badge/Badge_Bottom-PCB/Printing_Drucke_Schaltplan__1_.pdf) (note: no promises that this is final!).

## Is there a recommended dev board?
Since the BLE chip is very new, none of the card10 researches and travelers have found a suitable dev boards to recommend. We will try to share the research prototypes as widely as possible through hackerspaces. If you want to get your hands on a card10 prototype (we expect to start circulating them mid-May), first see if a hackerspace near you already has a card10logy department. If this is not the case, please get in touch via [Matrix Chat or IRC](/) to suggest opening a new card10logy department.

## How can I get a prototype?
You can find out if there's an ambassador in your city or a city near you. If there isn't maybe you want to become an ambassador yourself and help with the card10logix community.
More detailed info you can find [here](/en/prototype_distribution)

## What should I bring to cccamp 2019 so I can enjoy card10?
*I have noticed that the card10 needs some maintenance; most people carrying a card10 have brought their own tools for maintaining their card10, making it more individual, or adding functionality. In some places, maintenance stations are available, where card10's can be recharged, and tools and knowledge is shared*

According to current research, it is recommended to carry the following:

* A USB-C cable for charging and programming
* A screwdriver for assembling card10 (more details following soon)
* A needle for sewing to the card10 wristband
* A device capable of communicating via [Bluetooth Low Energy](/en/ble) (BLE)
