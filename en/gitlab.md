---
title: GitLab
---


## Post-Registration

Welcome to ccccamp badge card10 GitLab instance.

**1st**: *Check* your *mails*, links below won't work until verification.

### What's next

To get an understanding of card10 (cardio) checkout:

- our wiki [Wiki](https://card10.badge.events.ccc.de)
- explore our [GitLab Server](https://git.card10.badge.events.ccc.de/explore/groups).
   - also checkout our public repos at [`/card10`](https://git.card10.badge.events.ccc.de/open/), including source for the wiki.

### Profile Settings
Keep this tab open, accept your registration mail and you can go to this links.

- Updating Your [Profile Privacy](https://git.card10.badge.events.ccc.de/profile)
- Adding [Two-Factor/OTP](https://git.card10.badge.events.ccc.de/profile/two_factor_auth) 
- Change [E-Mail Notifications](https://git.card10.badge.events.ccc.de/profile/notifications)
- Add your [SSH Key](https://git.card10.badge.events.ccc.de/profile/keys) for `git clone...`
