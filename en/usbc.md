---
title: USB-C
---

# General use

You will need a USB-C cable to charge your badge. You can also update the firmware, mount it as a storage device to read files and flash software. We plan to provide most of these functions via ble as well, but a wired connection might still come handy.
The general USB-C use will work whichever way round the USB-C cable is plugged in.

# Full I/O

Traveler reports agree that the badge uses USB-C, the assumption is that this was chosen over USB-micro for the available range of extra pins. The reconstruction thus has some extra pins available, which would explain several observations of hardware interHacktions using USB-C.
 
**!Caution!** Most USB-C cables available for sale do not connect all pins of the connector. Whilst this is not a problem for general use, if you want to access the full available I/O contacts, it might be best to choose a cable from the suggestions listed at the bottom of the page.

**!Caution!** Card10 is still in the prototyping phase, bear in mind that hardware changes may still occur.

To use full I/O, there is a 'correct side up' for the USB-C cable. If the cable is plugged in the wrong way round, serial RX and TX are shorted together. So if you find yourself unsure, try sending some characters via serial console and see if you get them returned immediately; in that case the 'wrong' side is up.


## I2C
I2C ("i-squared-c" or "i-two-c") is a two wire protocol used for communication between 
## UART
## EKG

## 'confirmed' full pin cable options
Whilst this list does not guarantee you anything, these cables have been bought before and were found to have all contacts connected:
 * TODO
