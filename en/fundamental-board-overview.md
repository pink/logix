---
title: Fundamental board overview
---
The fundamental board is located just above the wristband; above it come the battery, the harmonic board, and the display. The harmonic board is connected to the fundamental board via the TOP board connector. 

Several interfaces for [interhacktions](/en/interhacktions) are included on the fundamental board. The travelers can be alerted to an event using the vibration motor. Screws through the EKG contacts are used both for attaching the wristband and for making contact with your skin for EKG measurements. Besides its use for interhacktions, the [usbC](/en/usbc) connector is also used for charging the battery and for updating the firmware.

A pdf version of the prototype fundamental board is available [here](https://gitlab.hamburg.ccc.de/card10/hardware/blob/master/badge/Badge_Bottom-PCB/Printing_Drucke_Schaltplan__1_.pdf)

![landmarks](/media/landmarks.png)
